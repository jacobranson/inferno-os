implement QuantaInheritance;
include "sys.m";
include "draw.m";


QuantaInheritance: module {
	init:	fn(nil: ref Draw->Context, nil: list of string);
};

init(nil: ref Draw->Context, nil: list of string)
{
	sys := load Sys Sys->PATH;

    spawn child(sys, 1);
	spawn child(sys, 2);
}

child(sys: Sys, number: int)
{
    sys->print("Created child %d\n", number);
	spawn subchild(sys, number, 1);
	spawn subchild(sys, number, 2);
}

subchild(sys: Sys, child: int, number: int)
{
	sys->print("Created subchild %d of child %d\n", number, child);
}
